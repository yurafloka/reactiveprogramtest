//
//  RxProperty.swift
//  FamilyApp
//
//  Created by Andy Chikalo on 8/8/18.
//  Copyright © 2018 Softbuddies. All rights reserved.
//

import Foundation
import RxSwift

/**
 Immutable counterpart of RxSwift Variable
 */
class Property<T> {
  private let variable: Variable<T>
  private let disposeBag: DisposeBag = DisposeBag()

  var value: T {
    return variable.value
  }

  init(variable: Variable<T>) {
    self.variable = variable
  }

  init(initialValue: T, observable: Observable<T>) {
    self.variable = Variable(initialValue)

    observable.subscribe(onNext: { [unowned self] in
      self.variable.value = $0
    }).disposed(by: disposeBag)
  }

  func asObservable() -> Observable<T> {
    return variable.asObservable()
  }
}
