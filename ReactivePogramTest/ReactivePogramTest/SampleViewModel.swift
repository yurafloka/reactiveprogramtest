//
//  SampleViewModel.swift
//  FamilyApp
//
//  Created by Andy Chikalo on 8/8/18.
//  Copyright © 2018 Softbuddies. All rights reserved.
//

import Foundation
import RxSwift

protocol SampleViewModelType {
  var items: Property<[String]> { get }
  func add()
}

class SampleViewModel: SampleViewModelType {
  var items: Property<[String]> { return Property(variable: itemsVar) }
  private let itemsVar = Variable<[String]>([])

  init() {
    itemsVar.value = ["4", "42", "7", "95", "13"]
  }

  func add() {
    itemsVar.value.append("\(arc4random() % 100)")
  }
}
