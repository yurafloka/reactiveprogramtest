//
//  SampleViewController.swift
//  FamilyApp
//
//  Created by Andy Chikalo on 8/8/18.
//  Copyright © 2018 Softbuddies. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SampleViewController: UIViewController {

  private struct Contants {
    static let cellIdentifier = "cell.sample"
  }

  @IBOutlet private weak var addButton: UIButton!
  @IBOutlet private weak var tableView: UITableView!

  private var viewModel: SampleViewModelType!  // nil
  private let disposeBag = DisposeBag()

  convenience init(viewModel: SampleViewModelType) {
    self.init()
    self.viewModel = viewModel
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    test()
    setupView()
    subscribeToViewModelUpdates()
  }

  public func test () {
    let subject = BehaviorSubject<String>(value: "Initial value")
    let observable : Observable<String> = subject

    subject.onNext("Not printed!")
    subject.onNext("Not printed!")
    subject.onNext("Printed!")

    observable
      .subscribe(onNext: { text in
        print(text)
      })
      .addDisposableTo(disposeBag)

    subject.onNext("Printed!")
  }

  func subscribeToViewModelUpdates() {
    viewModel.items.asObservable()
      .subscribe(onNext: { [weak self] _ in   // підписуюсь на потік, при додаванні нових елементів/значень буде вмконуватись релоадДата
        self?.tableView.reloadData()
      })
      .disposed(by: disposeBag)
  }

  private func setupView() {
    addButton.addTarget(self, action: #selector(addPressed), for: .touchUpInside)
    tableView.tableFooterView = UIView()
    tableView.register(UITableViewCell.self, forCellReuseIdentifier: Contants.cellIdentifier)
  }
  @objc
  private func addPressed() {
    viewModel.add()
  }
}

extension SampleViewController: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.items.value.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: Contants.cellIdentifier, for: indexPath)
    cell.textLabel?.text = viewModel.items.value[indexPath.row]
    return cell
  }
}

extension SampleViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    //TODO
  }
}
